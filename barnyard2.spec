# $Id$
# Snort.org's SPEC file for Snort

Summary: Snort Log Backend 
Name: barnyard2
Version: master
Source0: https://github.com/firnsy/barnyard2/archive/barnyard2-%{version}.tar.gz
Release: 1%{?dist}
License: GPL
Group: Applications/Internet
Url: http://www.github.com/firnsy/barnyard2

BuildRoot: %{_tmppath}/%{name}-%{version}-root
BuildRequires: libtool
BuildRequires: libpcap-devel
BuildRequires: libdnet-devel
BuildRequires: systemd
Requires: mysql python2-idstools
BuildRequires: mysql-devel
Requires: libdnet
BuildRequires: libdnet-devel
Requires: daq
BuildRequires: daq-devel

%description
Barnyard (with MySQL support) has 3 modes of operation:
One-shot, continual, continual w/ checkpoint.  In one-shot mode,
barnyard will process the specified file and exit.  In continual mode,
barnyard will start with the specified file and continue to process
new data (and new spool files) as it appears.  Continual mode w/
checkpointing will also use a checkpoint file (or waldo file in the
snort world) to track where it is.  In the event the barnyard process
ends while a waldo file is in use, barnyard will resume processing at
the last entry as listed in the waldo file.

%prep
#%setup -q -n barnyard2-v2-1.13.20181107git
%setup -q -n barnyard2-master

%build
./autogen.sh

%configure --with-mysql --with-mysql-libraries=/usr/lib64/mysql/

make

# Install
%install
%makeinstall 

install -d -p $RPM_BUILD_ROOT%{_sysconfdir}/{sysconfig,snort,snort.d} 
install -d -p $RPM_BUILD_ROOT%{_datadir}/snort
install -m 644 deploy/barnyard2.config $RPM_BUILD_ROOT%{_sysconfdir}/sysconfig/barnyard2
mv $RPM_BUILD_ROOT%{_sysconfdir}/barnyard2.conf $RPM_BUILD_ROOT%{_sysconfdir}/snort/
install -m 755 schemas/create_mysql $RPM_BUILD_ROOT%{_datadir}/snort/create_mysql
install -D -m 644 deploy/barnyard2.service %{buildroot}/%{_unitdir}/barnyard2.service
install -D -m 644 etc/gen-msg.map $RPM_BUILD_ROOT%{_sysconfdir}/snort.d/
mkdir -p /var/log/barnyard2

# Pre uninstall
%preun
%systemd_preun barnyard2.service

# Post install
%post
%systemd_post barnyard2.service

# Post uninstall
%postun
%systemd_postun_with_restart barnyard2.service

# Clean
%clean
if [ -d $RPM_BUILD_ROOT ] && [ "$RPM_BUILD_ROOT" != "/"  ] ; then
   rm -rf $RPM_BUILD_ROOT
fi

%files
%defattr(-,root,root)
%doc LICENSE doc/INSTALL doc/README.*
%attr(755,root,root) %{_bindir}/barnyard2
%attr(640,root,root) %config(noreplace) %{_sysconfdir}/snort/barnyard2.conf
%attr(644,root,root) %config(noreplace) %{_sysconfdir}/sysconfig/barnyard2
%attr(755,root,root) %{_datadir}/snort/create_mysql
%attr(644,root,root) %{_unitdir}/barnyard2.service
%attr(644,root,root) %{_sysconfdir}/snort.d/gen-msg.map

%changelog

* Sun Nov 04 2018 ClearFoundation <developer@clearfoundation.com>
- Build and artficially named 1.13.9 from master branch, heavily lifted from work from (ref github.com/firnsy/barnyard2.git):
- Bill Bernsen <bernsen@gmail.com>
- Brent Woodruff <brent@fprimex.com>
- Jason Haar <jhaar@sf.net>
- Ian Firns <firnsy@securixlive.com>
- Tom McLaughlin <tmclaugh@sdf.lonestar.org>
- Matthew Hall <matt@ecsc.co.uk>
- Fabien Bourdaire <fabien@ecsc.co.uk>
- Ralf Spenneberg <ralf@spenneberg.net>
